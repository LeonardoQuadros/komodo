$(document).on 'turbolinks:load', ->
  $('select').formSelect()
  $('.tooltipped').tooltip()
  $('.dropdown-filter-dropdown').remove()
  $('table').excelTableFilter()
  $('.dropdown-trigger').dropdown
    alignment: 'left'
    constrainWidth: false
    coverTrigger: false
  $('.modal').modal()
  $('.modal').css('min-height', $(window).height()/2)
  $('.tabs').tabs()
  $('.sidenav').sidenav()
  $('.collapsible').collapsible()
  $('.datepicker').datepicker dateFormat: 'dd/mm/yy'
  $('.parallax').parallax()
  $('.parallax-container').css('height', $(window).height()+64)
  $('#content_and_form').css('min-height', $(window).height())
  $('#modal').css('background', 'rgba(255, 255, 255, 0.8)')
  $('[data-toggle="tooltip"]').tooltip()
  M.updateTextFields();
  $('#modal_chat').css('min-height', $(window).height()*0.8)
  return

$(document).ajaxStop ->
  $('.dropdown-trigger').dropdown
  return

$(document).on 'turbolinks:before-cache', ->
  $('[data-toggle="tooltip"]').tooltip()
  $('select').formSelect('destroy')
  M.updateTextFields()
  return

App.chat = App.cable.subscriptions.create "ChatChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    M.toast
      html: 'Desconected.'
      classes: 'red'

  received: (data) ->
    tempo = 3000
    isOpen = $("#dialog").dialog("isOpen")
    if parseInt(data['destinatario_id']) == parseInt($("#current_user").attr("user")) or parseInt(data['destinatario_id']) == -1
      $('#dialog').dialog 'open'
      tempo = 5000
    if data['tempo']
      tempo = parseInt(data['tempo'])

    tipos = JSON.parse($("#current_user").attr("tipos"))
    if tipos.indexOf(data['tipo']) != -1 or parseInt(data['destinatario_id']) == parseInt($("#current_user").attr("user"))
      if tipos.indexOf(5) != -1
        M.toast
          html: data['message']
          classes: 'white grafit-text border'
          displayLength: tempo
          activationPercent: 0.01
      $('.messages').append '<p id="message_'+data['message_id']+'" class="cursor inspecionar_user_message span f10" message="'+data['message_id']+'" user="'+data['user_id']+'">'+data['message']+'</p>'
      $("#dialog").animate({ scrollTop: $('.messages').prop("scrollHeight")}, 1)
      if !isOpen
        $(".open_chat").addClass("animated")
        $(".open_chat > i.material-icons").removeClass("white-text")
        $(".open_chat > i.material-icons").addClass("red-text")
        return

  speak: (message) ->
    @perform 'speak', message: message, user_id: $("#current_user").attr('user')


$(document).on 'keypress', '[data-behavior~=chat_speaker]', (event) ->
  if event.keyCode is 13 # return = send
    App.chat.speak (event.target.value)
    event.target.value = ""
    event.preventDefault()
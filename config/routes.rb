Rails.application.routes.draw do


	devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: "register"}

	controller :pages do
		get '/home' => :home
		get '/profile' => :profile
	end

	root "pages#home"  
	get '*path' => redirect('/')

end
